//
//  DataErrorCode.swift
//  StarWars
//
//  Created by Administrador on 18/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

enum DataErrorCode: NSInteger {
    case networkUnavailable = 101
    case wrongDataFormat = 102
}
