//
//  CoreDataStack.swift
//  StarWars
//
//  Created by Administrador on 18/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import CoreData

let dataErrorDomain = "dataErrorDomain"

class CoreDataStack {
    
    private init() {}
    static let shared = CoreDataStack()
    
    lazy var  persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "StarWars")
        
        container.loadPersistentStores(completionHandler: { (persistentStoreDescription, error) in
            guard let error = error as NSError? else { return }
            fatalError("Ocorreu um problema ao tentar carregar o Armazenamento: \(error), \(error.userInfo)")
        })
        
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        container.viewContext.undoManager = nil
        container.viewContext.shouldDeleteInaccessibleFaults = true
        
        container.viewContext.automaticallyMergesChangesFromParent = true
        
        return container
    }()
}
